//public and private repos
var pubRepo = [];
var priRepo = [];
//after page load
$( document ).ready(function(){
  $.ajax({
 url: "https://raw.githubusercontent.com/Gopinath001/udts-w/reformat/x-udts.json",
 type: 'GET',
 success: processing,
 dataType: 'json'
  });
});

//parse json
function processing(data){
  for (a in data){
    if(data[a].r == 'pu'){
      pubRepo.push(a);
    }
    else if(data[a].r == 'pr'){
      priRepo.push(a);
    }
    else{
      console.log('error');
    }
  }
  fetchRepo();
}
var iReq = [];
var iq = [];
function fetchRepo(){ 
  for(var i =0; i< pubRepo.length; i++){
    iq[i] = $.ajax({
 url: "https://api.bitbucket.org/2.0/repositories/Bluepie/"+ pubRepo[i] + "/commits",
 type: 'GET',
 success: aH,
 dataType: 'json'
  });
  }
}

function aH(data){
  iReq.push(data);
  if((iq)[pubRepo.length-1].statusText=="success")
  attachHtml();
}

function attachHtml(){
  if(pubRepo.length == iReq.length){
    //sorting repos by date
    iReq.sort(function(a,b){
      var dateA = new Date(a.values["0"].date);
      var dateB = new Date(b.values["0"].date);
      return dateB-dateA;     
    });
    for(var i =0 ; i< iReq.length ; i++){
      var nameOfRepo = iReq[i].values["0"].repository.name;
      var dateOfLastCommit = new Date(iReq[i].values["0"].date);
      var last_commit = iReq[i].values["0"].summary.html;
      dateOfLastCommit = dateOfLastCommit.toString().substr(0,(dateOfLastCommit.toString().indexOf('GMT')));
      $("#ul").append('<li><span class="dot"></span><span class="cmt_msg"><span>'+last_commit+'</span></span><span class="t-repo"><span class="date"><span>'+dateOfLastCommit+'</span></span>'+ nameOfRepo+'</span><span>');
    }
  }
  else{
    console.log('request not complete' + pubRepo.length + ' !' + iReq.length);
  }
}